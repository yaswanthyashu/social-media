package socialmedia;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Implementing a class to display users messages
 * @author yashu1
 */
public class UserCollections
{
	private ArrayList<User> userList;
	private ArrayList<Message> sendMessagesList;
	
	public UserCollections() {
		this.userList=new ArrayList<User>();
	}
	/**
	 * To create createNewUser method using parameters
	 * by using ArrayList and addUser
	 * @param name
	 * @param phoneNo
	 * @param password
	 * @return
	 */
	public boolean createNewUser(String name, String phoneNo, String password)
	{
		try
		{
			User addUser = new User(name, phoneNo, password);
			userList.add(addUser);
			return true;
		}catch(Exception e) {
		
		return false;	
	}
}
	/**
	 * To create a method  for printAllusers 
	 * by sing Iterator to print the currentUser
	 */
	public void printAllusers() {
		for (Iterator<User> iter = userList.iterator();
				iter.hasNext();) {
			User currentUser = (User)iter.next();
			System.out.println(currentUser);
			}
	}
	/**
	 * To create a method for sendMessage
	 * By using try and catch blocks with sendMessagesList 
	 * @param sender
	 * @param reciever
	 * @param msgbody
	 * @return
	 */
	public boolean sendMessage(String sender, String reciever, String msgbody, LocalDateTime timeStamp) 
	{
	
			Message addSendMessages = new Message(sender, reciever, msgbody,timeStamp);			
			if(sender.contains(reciever))
			{
			sendMessagesList.add(addSendMessages);
			} else 
			{
				System.out.println("failed to send ");
			}
			return false;		
    }
}

