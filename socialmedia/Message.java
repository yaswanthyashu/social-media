package socialmedia;
import java.time.LocalDateTime;
import java.util.ArrayList;
/**
 * Implementing a class to provide information for messages
 * @author yashu1
 */
public class Message 
   {
	ArrayList<String> messageList = new ArrayList<String>();
    String sender;
    String reciever;
    String msgbody;
    LocalDateTime timeStamp;
/**
 * Here is a constructor with parameters 
 * @param sendmsg
 * @param recvmsg
 * @param msgbody
 * @param timeStamp
 */
public Message(String sender, String reciever, String msgbody, LocalDateTime timeStamp)
   {
	this.sender = sender;  //using this statement
	this.reciever = reciever;   //using this statement
	this.msgbody = msgbody;      //using this statement
	this.timeStamp = timeStamp;
	//System.out.println(this);
   }
/**
 * override the default toString method of Message 
 * so that message can be printed in human readable format
 */
@Override
public String toString()
   {
	String returnToMe=" ";
	returnToMe += "------------------------------"    +"\n";
	returnToMe += "sender      :      "+this.sender   +"\n";
	returnToMe += "reciever    :      "+this.reciever +"\n";
	returnToMe += "msgbody     :      "+this.msgbody  +"\n";
	returnToMe += "Local Time  :      "+this.timeStamp+"\n";
	return returnToMe;
   }	
}

