package socialmedia;
/**
 * Implementing a class to provide the details of user
 * @author yashu1
 */
public class User {
		public String name;     	
	    public String phonenum;		
		public String password;		
		/**
		 * created a constructor with parameters
		 * @param name
		 * @param phonenum
		 * @param password
		 * @throws Exception
		 */
	   public User(String name, String phonenum, String password) throws Exception
	   {
			if(name.trim().length() < 2 || name.trim().length() > 20) 
			{
				// validation of name failure
				throw new Exception("Name is too short.");
			}
			else if(phonenum.trim().length() < 2 || phonenum.trim().length() > 260) 
			{
				// validation of phone failure
				throw new Exception("phone is too short");
			}
			else if(password.length()<1 || password.length()>9)
			{
				// validation of price failure
				throw new Exception("password is too low or negative");
			} else {
		}
			this.name = name;
			this.phonenum = phonenum;
			this.password = password;
		}
		@Override
		public String toString() 
		{
			String userreturn = "";
			userreturn += "Personal information"+"\n";
			userreturn += "name     :   "+this.name+"\n";
			userreturn += "phone    :   "+this.phonenum+"\n";
			userreturn += "password :   "+this.password+"\n";			
			return userreturn;
		}
	}
